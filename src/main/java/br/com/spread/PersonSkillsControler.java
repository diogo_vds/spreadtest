 package br.com.spread;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.spread.helper.PersonHelper;
import br.com.spread.model.Person;
import br.com.spread.model.Skill;
import br.com.spread.repository.PersonRepository;
import br.com.spread.repository.SkillRepository;
import br.com.spread.vo.PersonVO;

@RestController
public class PersonSkillsControler {
	
	@Autowired
	private PersonRepository personRepository;
	@Autowired
	private SkillRepository skillRepository;
	
    
    @RequestMapping("/savePerson")
    public String savePerson(@RequestParam(value="name") String name) {
        Person person = new Person(name);
        personRepository.save(person);
        return "Person saved sucesfull";
    }
    
    @RequestMapping("/saveSkill")
    public String saveSkill(@RequestParam(value="name") String skill) {
    	Skill sk = new Skill();
    	sk.setSkill(skill);
        skillRepository.save(sk);
        return "Skill saved sucesfull";
        
    }
    
    @RequestMapping("/savePersonSkill")
    public String savePersonSkill(@RequestParam(value="idPerson") Long idPerson, @RequestParam(value="idSkill") Long idSkill) {
    	Person p = personRepository.findOne(idPerson);
    	Skill sk = skillRepository.findOne(idSkill);
    	p.getSkills().add(sk);
    	personRepository.flush();
    	return "Person Skill add sucesfull";
    }
    
    @RequestMapping("/getPersonById")
    public PersonVO getPersonById(@RequestParam(value="id") Long id) {       
    	PersonVO vo = PersonHelper.personEntitytoVO(personRepository.findOne(id));    	
        return vo;
    }
    
    @RequestMapping("/getAllPerson")
    public List<String> getAllPerson() {
    	List<Person> list = personRepository.findAll();
    	List<String> persons = new ArrayList<String>();
    	for(Person p : list){
    		persons.add(p.getName());
    	}
        return persons;
    }
    
    @RequestMapping("/getAllPersonsAndSkills")
    public List<PersonVO> getAllPersonsAndSkills() {
    	List<Person> list = personRepository.findAll();
    	List<PersonVO> persons = new ArrayList<PersonVO>();
    	for(Person p : list){
    		persons.add(PersonHelper.personEntitytoVO(p));
    	}
        return persons;
    }
  
    @RequestMapping("/getSkillsFromPerson")
    public List<String> getSkillsFromPerson(@RequestParam(value="id") Long id) {
    	Person p = personRepository.findOne(id);
    	List<String> persons = new ArrayList<String>();
    	for(Skill sk : p.getSkills()){
    		persons.add(sk.getSkill());
    	}
        return persons;
    }
    
    @RequestMapping("/removeSkill")
    public String removeSkill(@RequestParam(value="id") Long id) {    	
    	skillRepository.delete(id);
        return "Skill removed sucessfull!";
    }
    
    @RequestMapping("/removePerson")
    public String removePerson(@RequestParam(value="id") Long id) {
    	personRepository.delete(id);    	
        return "Person removed sucessfull!";
    }
    
    @RequestMapping("/updatePerson")
    public String updatePerson(@RequestParam(value="id") Long id, @RequestParam(value="name") String name) {
    	Person p = personRepository.findOne(id);
    	p.setName(name);
    	personRepository.saveAndFlush(p);
        return "Person updated sucessfull!";
    }
    
    @RequestMapping("/updateSkill")
    public String updateSkill(@RequestParam(value="id") Long id, @RequestParam(value="name") String name) {
    	Skill s = skillRepository.findOne(id);
    	s.setSkill(name);
    	skillRepository.saveAndFlush(s);    
        return "Skill updated sucessfull!";
    }
    
    
}
