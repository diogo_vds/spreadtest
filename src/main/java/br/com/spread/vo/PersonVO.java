package br.com.spread.vo;

import java.util.List;

public class PersonVO {

	private Long id;
	private String name;
	private List<SkillVO> skills;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<SkillVO> getSkills() {
		return skills;
	}
	public void setSkills(List<SkillVO> skills) {
		this.skills = skills;
	}
}
