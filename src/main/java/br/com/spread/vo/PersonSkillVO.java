package br.com.spread.vo;

public class PersonSkillVO {
	
	private Long id;
	private Long idPerson;
	private Long idSkill;
	private String namePerson;
	private String nameSkill;
			
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdPerson() {
		return idPerson;
	}
	public void setIdPerson(Long idPerson) {
		this.idPerson = idPerson;
	}
	public Long getIdSkill() {
		return idSkill;
	}
	public void setIdSkill(Long idSkill) {
		this.idSkill = idSkill;
	}
	public String getNamePerson() {
		return namePerson;
	}
	public void setNamePerson(String namePerson) {
		this.namePerson = namePerson;
	}
	public String getNameSkill() {
		return nameSkill;
	}
	public void setNameSkill(String nameSkill) {
		this.nameSkill = nameSkill;
	}

}
