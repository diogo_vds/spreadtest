package br.com.spread.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@EntityScan(basePackages = "br.com.spread.model")
@EnableJpaRepositories(basePackages = "br.com.spread.repository")
@SpringBootApplication(scanBasePackages = "br.com.spread") 	
public class TesteJavaRestApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(TesteJavaRestApplication.class, args);
    }
}