package br.com.spread.helper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import br.com.spread.model.Person;
import br.com.spread.model.Skill;
import br.com.spread.vo.PersonVO;
import br.com.spread.vo.SkillVO;

public class PersonHelper {

	public static Person personVOtoEntity(PersonVO vo){		
		Person enti = new Person();		
		enti.setId(vo.getId());
		enti.setName(vo.getName());		
		return enti;		
	}
	
	public static PersonVO personEntitytoVO(Person enti){		
		PersonVO vo = new PersonVO();		
		vo.setId(enti.getId());
		vo.setName(enti.getName());
		List<SkillVO> list = new ArrayList<SkillVO>();
		Iterator<Skill> i = enti.getSkills().iterator();		
		while(i.hasNext()){
			Skill sk = (Skill) i.next();
			SkillVO skillVO = new SkillVO();
			skillVO.setId(sk.getId());
			skillVO.setName(sk.getSkill());
			list.add(skillVO);
		}
		vo.setSkills(list);
		return vo;		
	}
	
}
