package br.com.spread.helper;

import br.com.spread.model.Skill;
import br.com.spread.vo.SkillVO;

public class SkillHelper {

	public static Skill personVOtoEntity(SkillVO vo){		
		Skill enti = new Skill();		
		enti.setId(vo.getId());
		enti.setSkill(vo.getName());		
		return enti;		
	}
	
	public static SkillVO personEntitytoVO(Skill enti){		
		SkillVO vo = new SkillVO();		
		vo.setId(enti.getId());
		vo.setName(enti.getSkill());	
		return vo;		
	}
}
