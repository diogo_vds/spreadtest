package br.com.spread.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.spread.model.Person;
import br.com.spread.model.Skill;

public interface SkillRepository extends JpaRepository<Skill, Long>{
	

	/**
	 * Find person by id
	 * 
	 * @param autor
	 * @return lista de livros
	 */
	 Skill findById(String autor);

	/**
	 * Find all person
	 * 
	 * @param titulo
	 * @return lista de livros
	 */
	 List<Skill> findAll();

	
	
	
}
