package br.com.spread.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.spread.model.Person;

public interface PersonRepository extends JpaRepository<Person, Long>{
	

	/**
	 * Find person by id
	 * 
	 * @param autor
	 * @return lista de livros
	 */
	 Person findById(String autor);

	/**
	 * Find all person
	 * 
	 * @param titulo
	 * @return lista de livros
	 */
	 List<Person> findAll();

	
	
	
}
