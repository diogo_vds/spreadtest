# README #


### 1) How to Compile project? ###
- run following command: mvn spring-boot:run
- The following services are available:

   http://localhost:8080/savePerson?name=Person
   http://localhost:8080/saveSkill?name=Programmer    
   http://localhost:8080/savePersonSkill?idPerson=1&idSkill=1
   http://localhost:8080/getPersonById?id=1    
   http://localhost:8080/getAllPerson    
   http://localhost:8080/getAllPersonsAndSkills  
   http://localhost:8080/getSkillsFromPerson?id=1    
   http://localhost:8080/updatePerson?id=1&name=Employ  
   http://localhost:8080/updateSkill?id=1&name=JrProgrammer
   http://localhost:8080/removeSkill?id=1    
   http://localhost:8080/removePerson?id=1 
	
### 2) How to run the suite of automated tests? ###
- Unable to implement automated tests in time.