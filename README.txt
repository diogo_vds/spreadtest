1) How to Compile project?
- run following command: mvn spring-boot:run
- The following services are available:

   http://localhost:8080/savePerson?name=Diogo    
   http://localhost:8080/saveSkill?name=Programmer    
   http://localhost:8080/savePersonSkill?idPerson=3&idSkill=4    
   http://localhost:8080/getPersonById?id=1    
   http://localhost:8080/getAllPerson    
   http://localhost:8080/getAllPersonsAndSkills  
   http://localhost:8080/getSkillsFromPerson?id=1    
   http://localhost:8080/updatePerson?id=1&name=DiogoSantos    
   http://localhost:8080/updateSkill?id=1&name=SmartProgrammer
   http://localhost:8080/removeSkill?id=4    
   http://localhost:8080/removePerson?id=7  
    
	
2) How to run the suite of automated tests?
- Unable to implement automated tests in time.
3) Mention anything that was asked but not delivered and why, and any additional comments.

 - "The application must have a stateless API and use an embedded in-memory H2 database to store data" - It was not possible to implement with an embedded in-memory H2 database to store data because I had no prior experience, so I used the postgres database.
 
 
